﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WindowsFormsApplication2;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
           string s= Form1.Change("АаБб??*76YYThgtЯ3",1,1);
            Assert.AreEqual("БбВв??*76YYThgtА3", s);

            
        }
        [TestMethod]
        public void TestMethod2()
        {
            string s = Form1.Change("АаБб??*76YYThgtЯ3", 0, 31);


            Assert.AreEqual("ВвГг??*76YYThgtБ3", s);
        }

        [TestMethod]
        public void TestMethod3()
        {
            string s = Form1.Change("АаБб??*76YYThgtЯ3", 2, 17);


           
            Assert.AreEqual("РрСс??*76YYThgtП3", s);
        }
        [TestMethod]
        public void TestMethod4()
        {
            string s = Form1.Change("АаБб??*76YYThgtЯ3", 2, 17);


            Assert.AreEqual("ПпРр??*76YYThgtО3", s);
            
        }

        [TestMethod]
        public void TestMethod5()
        {
            string s = Form1.Change("АаБб??*76YYThgtЯ3", 5, 17);


            Assert.AreEqual("ПпРр??*76YYThgtО3", s);

        }
        
    }
}
